package ca.ualberta.cs.lonelytwitter.lonelytweet;

import java.util.Date;

public class NormalLonelyTweet extends LonelyTweet {

	// Removed unused constructor

	public NormalLonelyTweet(String text, Date date) {
		this.tweetDate = date;
		this.tweetBody = text;
	}

}